import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Home, Cars } from './pages';
import  NavBar  from './components/Navbar';
import  Footer  from './components/Footer';
import './App.css';

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route exact path="/Cars" element={<Cars />} />
      </Routes>
      <Footer />  
    </BrowserRouter>
  );
}

export default App;
