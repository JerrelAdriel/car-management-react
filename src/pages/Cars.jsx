import '../assets/css/module.style.css';
import {Form} from 'react-bootstrap';
import {img_car} from '../assets/img';
function Cars() {
    return(
        <div>
             <header>
                <div class ="desc-header bg-light">
                    <div class="container-fluid">
                        <div class="row pt-5 align-items-center">
                            <div class="col-lg">
                                <p class="text1-header">Sewa & Rental Mobil Terbaik di kawasan Medan</p>
                                <p class="text" >Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam. </p>
                            </div>
                            <div class="col img-car" >
                                <img src={img_car} alt="mobil" ></img>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <section id="filter">
            <div class="container px-5" >
            <div class="card justify-content-between">
                <Form class="d-grid form" >
                <div class="row ">
                    <div class="col-lg">
                    <div class="form-group">
                        <label for="" class="form-label">Tipe Driver</label>
                        <select class="form-select form-item" id="tipe-driver" disabled>
                        <option value="" disabled selected hidden>Pilih Tipe Driver</option>
                        <option>Dengan Sopir</option>
                        <option>Tanpa Sopir (Lepas Kunci)</option>
                        </select>
                    </div>
                    </div>
                    <div class="col-lg">
                    <div class="form-group">
                        <label for="" class="form-label">Tanggal</label>
                        <input type="date" id="tanggal" placeholder="Pilih Tanggal" class="form-control form-item " required/>
                    </div>
                    </div>
                    <div class="col-lg">
                    <div class="form-group">
                        <label for="" class="form-label">Waktu Jemput/Ambil</label>
                        <input type="time" id="waktu-jemput" placeholder="Pilih Waktu" class="form-control form-item" required/>
                    </div>
                    </div>
                    <div class="col-lg">
                    <div class="form-group">
                    <label for="" class="form-label">Jumlah Penumpang (optional)</label>
                    <input type="text" id="jumlah-penumpang" placeholder="Jumlah Penumpang " class="form-control form-item"/>
                    </div>
                    </div>
                    <div class="col-sm-2 py-4">
                        <button id="btn-cari" class="btn btn-success" type="submit" >Cari Mobil</button>
                    </div>
                </div>
                </Form>
            </div>
            </div>

        </section>
        <section id="show-cars">
            <div class="container px-lg-5" id="cars">
            <div class="row" id="cars-container"></div>
            <button id="clear-btn" >Clear</button>
            </div>
        </section>
        </div>
  
    );
}
export default Cars;