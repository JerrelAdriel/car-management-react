import {icon_facebook, icon_instagram, icon_mail, icon_twitter, icon_twitch} from '../assets/img';
import '../assets/css/module.style.css';
function Footer() {
    return(
        <footer>
          <div class="container">
              <div class="row">
                <div class="col-md-4 col-lg-3 ">
                  <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                  <p>binarcarrental@gmail.com</p>
                  <p>081-233-334-808</p>
                </div>
                <div class="col-md-2 col-lg-2 footer-nav" >
                  <a class="nav-link text" href="#section2">Our services</a> <br/>
                  <a class="nav-link text" href="#section3">Why Us</a><br/>
                  <a class="nav-link text" href="#section4">Testimonial</a><br/>
                  <a class="nav-link text" href="#section6">FAQ</a>
                </div>
                <div class="col-md-4 col-lg-3 icon">
                  <p>Connect with us</p>
                  <img src={icon_facebook} alt="icon_facebook" />
                  <img src={icon_instagram} alt="icon_instagram" />
                  <img src={icon_twitter} alt="icon_twitter" />
                  <img src={icon_mail} alt="icon_mail" />
                  <img src={icon_twitch} alt="icon_twitch" />
                </div>
                <div class="copyright col-md-2 col-lg-4">
                  <p>Copyright Binar 2022</p>
                  <div class="rectangle"></div>
                </div>
              </div>
            </div>
    </footer>
    );
}

export default Footer;