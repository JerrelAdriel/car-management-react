import '../assets/css/module.style.css';
import { Nav } from "react-bootstrap";
import {logo} from '../assets/img';

const NavBar = () => {
    return(
        <header>
             <Nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
              <div class="container">
                        <a className="navbar-brand" href="#Home">
                        <img src={logo} alt="logo"></img>
                        </a>
                      <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight" >
                        <span class="navbar-toggler-icon "></span>
                      </button>
                      <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
                        <div class="offcanvas-header">
                          <h5 id="offcanvasRightLabel">BCR</h5>
                          <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                        </div>
                        <div class="offcanvas-body">
                          <div class="navbar-nav ms-auto">                      
                              <a class="nav-link active text-dark " href="#service">Our Service</a>                               
                              <a class="nav-link text-dark" href="#whyus">Why Us</a>                         
                              <a class="nav-link text-dark" href="#testimonial">Testimonial</a>                                
                              <a class="nav-link text-dark" href="#FAQ">FAQ</a> 
                              <button class="btn btn-success btn-outline-none btn-style" type="submit">Register</button>
                          </div>
                        </div>
                      </div>
                    </div>
                </Nav>
    </header>   
    );
}

export default NavBar;

